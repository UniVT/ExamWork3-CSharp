﻿/*
 * 1. Съставете програма за манипулиране на месечни данни за изработени часове по проекти на служители на софтуерна компания.
 * За целта направете два метода (подпрограми):
 * а) за въвеждане на часовете, които даден служител е изработил за всеки ден в едномерен масив от цели неотрицателни числа;
 * б) за изчисляване на броя часове, които даден служител е прекарал в работа по проекти в извънработно време 
 * (приема се, че се работи с 8 часов работен ден).
 * 2. В главния метод въведете цяло число n≤31 (брой работни дни в месеца) и три едномерни масива A[n], B[n] и C[n], 
 * моделиращи изработените часове за дадения месец от трима различни служители на компанията. 
 * За всеки служител изчислете и отпечатайте броя часове, които той е прекарал в работа по проекти в извънработно време.
 * 3. За всеки служител намерете и отпечатайте номерата на дните, в които е работил извънредно.
 */

namespace ExamWork3
{
    using System;

    class Program
    {
        // 1a)
        static int[] EnterData(int[] month)
        {
            for (int x = 0; x < month.Length; x++)
            {
                Console.Write("Въведете изработени часове за ден {0}:", x+1);
                month[x] = Int32.Parse(Console.ReadLine());
            }

            return month;
        }

        // 1b)
        static int ExtraWork(int[] month)
        {
            int sum = 0;

            foreach (int day in month)
            {
                if (day > 8)
                {
                    // sum = sum + 
                    sum += day - 8;
                }
            }

            return sum;
        }

        static void Main(string[] args)
        {
            // 1)
            int n;
            try
            {
                Console.Write("Моля веведете брой дни в месеца: ");
                n = Int32.Parse(Console.ReadLine());
            }
            catch (System.FormatException ex)
            {
                Console.WriteLine(ex.Message);
                n = 30;
            }
            
            if (n >= 28 && n <= 31)
            {
                int[] A = new int[n];
                int[] B = new int[n];
                int[] C = new int[n];

                Console.Clear();
                EnterData(A);
                EnterData(B);
                EnterData(C);

                // 2)
                Console.WriteLine();
                Console.WriteLine("Служител 1 има {0} часа извънреден труд за месеца.", ExtraWork(A));

                Console.WriteLine();
                Console.WriteLine("Служител 2 има {0} часа извънреден труд за месеца.", ExtraWork(B));

                Console.WriteLine();
                Console.WriteLine("Служител 3 има {0} часа извънреден труд за месеца.", ExtraWork(C));

                // 3)
                Console.WriteLine();
                for (int x = 0; x < A.Length; x++)
                {
                    if (A[x] > 8)
                    {
                        Console.Write("Служител 1 е работил извънредно на {0}-то число от месеца. ", x+1);
                        Console.WriteLine("Извънреден труд: {0} часа.", A[x] - 8);
                    }
                }
            }
        }
    }
}
